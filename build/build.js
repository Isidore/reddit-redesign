require('shelljs/global');
env.NODE_ENV = 'production';

var path = require('path');
var config = require('../config');
var ora = require('ora');
var webpack = require('webpack');
var webpackConfig = require('./webpack.prod.conf')

console.log(
  `  Tip:
     Built files are meant to be served over an HTTP server.
     Opening index.html over file:// wont worl.
  `
);

var spinner = ora('building for production...');
spinner.start()

var assetsPath = path.join(config.build.assetsRoot, config.build.assetsSubDirectory);
rm('-rf', assetsPath);
mkdir('-p', assetsPath);

webpack(webpackConfig, function(err, stats) {
  spinner.stop();
  if (err) throw err;

  process.stdout.write(stats.toString({
    colors: true,
    modules: true,
    children: false,
    chunks: false,
    chunkModule: false
  }) + '\n');
});
