var path = require('path');
var webpack = require('webpack');
var config = require('../config');
var utils = require('./utils');
var autoprefixer = require('autoprefixer');

var projectRoot = path.resolve(__dirname, '../');
var env = process.env.NODE_ENV;

var cssSourceMapDev = (env === 'development' && config.dev.cssSourceMap);
var cssSourceMapProd = (env === 'production' && config.build.cssSourceMap);
var useCssSourceMap = cssSourceMapDev || cssSourceMapProd;

module.exports =
{ entry:
    { polyfills: './src/polyfills.ts'
    , app: './src/main.ts'
    }
, output:
    { path: config.build.assetsRoot
    , publicPath: process.env.NODE_ENV === 'production'
        ? config.build.assetsPublicPath
        : config.dev.assetsPublicPath
    , filename: '[name].js'
    }

, resolve:
    { extensions: ['.js', '.ts', '.scss', '.json']
    , modules: [path.join(__dirname, '../node_modules')]
    , alias:
        { 'src': path.resolve(__dirname, '../src')
        , 'core': path.resolve(__dirname, '../src/core')
        , 'utils': path.resolve(__dirname, '../src/utils')
        , 'pages': path.resolve(__dirname, '../src/pages')
        , 'config': path.resolve(__dirname, '../src/config')
        , 'layouts': path.resolve(__dirname, '../src/layouts')
        , 'services': path.resolve(__dirname, '../src/services')
        , 'components': path.resolve(__dirname, '../src/components')

        , 'styles': path.resolve(__dirname, '../src/styles')
        , 'static': path.resolve(__dirname, '../src/static')
        }
    }

, module:
    { rules:
      [
        { test: /\.ts$/
        , use:
            [ { loader: '@angularclass/hmr-loader' }
            , { loader: 'awesome-typescript-loader'
              , options: { configFileName: 'tsconfig.webpack.json' }
              }
            , { loader: 'angular2-template-loader' }
            ]
        , exclude: [/\.(spec|e2e)\.ts$/]
        }
        ,
        { test: /\.(scss|css)$/
        , use:
            [ { loader: 'to-string-loader' }
            , { loader: 'css-loader' }
            , { loader: 'postcss-loader'
              , options:
                { plugins: [ autoprefixer({ browsers: ['last 3 versions'] }) ]
                }
              }
            , { loader: 'sass-loader' }
            ]
        , exclude: [ utils.srcPath('styles') ]
        }
        ,
        { test: /\.html$/
        , use: 'raw-loader'
        }
        ,
        { test: /\.(ico|webp|png|jpe?g|gif|svg)(\?.*)?$/
        , loader: 'url-loader'
        , options:
          { limit: 10000
          , name: utils.assetsPath('images/[name].[hash:7].[ext]')
          }
        }
        ,
        { test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/
        , loader: 'url-loader'
        , options:
          { limit: 10000
          , name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
          }
        }
      ]
    }

, plugins:
    [ new webpack.optimize.CommonsChunkPlugin({
        name: 'polyfills',
        chunks: ['polyfills']
      })

    , new webpack.ContextReplacementPlugin(
        /angular(\\|\/)core(\\|\/)@angular/,
        utils.srcPath(''), { }
      )
    ]
};
