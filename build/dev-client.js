require('eventsource-polyfill');
var hotClient = require('webpack-hot-middleware/client?noInfo=true&reload=true');

hotClient.subscribe(function(ev) {
  if (ev.action === 'reload')
    window.location.reload();
});
