var webpack = require('webpack');
var merge = require('webpack-merge');
var autoprefixer = require('autoprefixer');

var FriendlyErrors = require('friendly-errors-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var utils = require('./utils');
var config = require('../config');
var baseWebpackConfig = require('./webpack.base.conf');


Object.keys(baseWebpackConfig.entry).forEach(function(name) {
  baseWebpackConfig.entry[name] = ['./build/dev-client'].concat(baseWebpackConfig.entry[name]);
});

module.exports = merge(baseWebpackConfig, {
  module: {
    rules:
    [
      { test: /\.(scss|css)$/
      , use:
          [ { loader: 'style-loader' }
          , { loader: 'css-loader' }
          , { loader: 'postcss-loader'
            , options:
              { plugins: [ autoprefixer({ browsers: ['last 3 versions'] }) ]
              , sourceMap: config.dev.cssSourceMap
              , sourceMapContents: false
              }
            }
          , { loader: 'sass-loader'
            , options: { sourceMap: config.dev.cssSourceMap }
            }
        ]
      , include: [ utils.srcPath('styles') ]
      }
    ]
  },

  // eval-source-map is faster for development
  devtool: '#eval-source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),

    new FriendlyErrors(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/index.html',
      inject: true
    })
  ]
});
