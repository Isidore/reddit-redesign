var path = require('path');
var express = require('express');
var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');

var webpackConfig = require('./webpack.dev.conf');
var config = require('../config');


if (!process.env.NODE_ENV)
  process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV);

var app = express();
var compiler = webpack(webpackConfig);

var PORT = process.env.PORT || config.dev.port;
var URI = 'http://localhost:' + PORT

var staticPath = path.posix.join(
  config.dev.assetsPublicPath,
  config.dev.assetsSubDirectory
);

var hotMiddleware = webpackHotMiddleware(compiler, {
  log: () => { }
});

var devMiddleware = webpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  quiet: true
});

// Update the page whenever the app html template file changes
compiler.plugin('compilation', function(compilation) {
  compilation.plugin('html-webpack-plugin-after-emit', function(data, cb) {
    hotMiddleware.publish({ action: 'reload' });
    cb();
  });
});

devMiddleware.waitUntilValid(function() {
  console.log('> Listening at ' + URI + '\n');
});


// Handle fallback for HTML5 history API
app.use(require('connect-history-api-fallback')())

// Serve webpack bundle output
app.use(devMiddleware);

// Enable hot-reload and state-preserving compilation error display
app.use(hotMiddleware);

// Serve pure static assets
app.use(staticPath, express.static('./assets'));

module.exports = app.listen(PORT, function(err) {
  if (err) {
    console.log(err);
    return;
  }
});
