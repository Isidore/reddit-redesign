import { Component, Input, Output, EventEmitter } from '@angular/core';

import { PostsService } from 'services/posts-service/posts.service';

@Component({
  selector: 'comment-editor',
  templateUrl: './comment-editor.component.html',
  styleUrls: [ './comment-editor.component.scss' ]
})

export default class CommentEditorComponent {
  @Input() parentId: string;
  @Input() isChild: boolean;

  @Output() onCommentSubmitted = new EventEmitter<object>();
  @Output() onCancel = new EventEmitter<void>();

  private editorHasFocus: boolean;
  private comment = { thing_id: '' , text: '' };

  constructor( private postsService: PostsService ) { }

  submitComment(): void {
    this.comment.thing_id = this.parentId;

    this.postsService.submitComment(this.comment).subscribe((data) => {
      console.log("submitComment.data: ", data);

      this.onCommentSubmitted.emit(data.data.things[0]);
      this.resetComment();
    });
  }

  emitCancel(): void {
    this.onCancel.emit();
  }

  collapseEditor(): void {

  }

  resetComment(): void {
    this.comment = { thing_id: '', text: '' };
  }
}
