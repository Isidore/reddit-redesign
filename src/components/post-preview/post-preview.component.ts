import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { PostsService } from 'services/posts-service/posts.service';

import { abbreviate, numToBoolean } from 'utils/utils';

const moment = require('moment');

@Component({
  selector: 'post-preview',
  templateUrl: './post-preview.component.html',
  styleUrls: [ './post-preview.component.scss' ]
})

export default class PostPreviewComponent {
  @Input() post;

  @Output() onPostAction = new EventEmitter<object>();

  constructor(
    private sanitizer: DomSanitizer,
    private postsService: PostsService
  ) {}

  get postScore() {
    return abbreviate(this.post.score, 1, 1);
  }

  get relativeCreationDate() {
    return moment(this.post.created_utc * 1000).fromNow();
  }

  getMedia(): SafeHtml {
    var txt = document.createElement('textarea');
    txt.innerHTML = this.post.secure_media_embed.content;

    return this.sanitizer.bypassSecurityTrustHtml(txt.value);
  }

  getPostImage(): string {
    return this.post.preview.images[0].variants.gif
      ? this.post.preview.images[0].variants.gif.source.url
      : this.post.preview.images[0].source.url;
  }

  getSelfText(): string {
    var txt = document.createElement('textarea');
    txt.innerHTML = this.post.selftext_html;
    return txt.value;
  }

  videoSource(): string {
    return this.post.media.reddit_video.scrubber_media_url;
  }

  votePost(dir) {
    var previousState = this.post.likes;
    this.post.likes = dir === 0 ? null : numToBoolean(dir);

    this.postsService.vote(this.post.name, dir)
      .subscribe(() => {}, (err) => this.post.likes = previousState);
  }

  savePost(isSaved:boolean = true): void {
    let previousState = this.post.saved;
    let saveAction = isSaved ? 'saveThing' : 'unsaveThing';
    this.post.saved = isSaved;

    this.postsService[saveAction](this.post.name)
      .subscribe(() => {}, (err) => this.post.saved = previousState);
  }

  emitPostAction(action: string): void {
    this.onPostAction.emit({ action, thing: this.post });
  }
}
