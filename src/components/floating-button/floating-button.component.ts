import { Component, Input } from '@angular/core';

@Component({
  selector: 'floating-button',
  templateUrl: './floating-button.component.html',
  styleUrls: [ './floating-button.component.scss' ]
})

export default class FloatingButtonComponent {
  @Input('label') buttonLabel: string;
}
