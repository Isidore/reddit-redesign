import { Component, Input, Output, EventEmitter } from '@angular/core';
import { isEmpty } from 'underscore';

import { PostsService } from 'services/posts-service/posts.service';

import { abbreviate, numToBoolean, capitalizeFirstLetter } from 'utils/utils';

var moment = require('moment');

@Component({
  selector: 'post-feed-card',
  templateUrl: './post-feed-card.component.html',
  styleUrls: [ './post-feed-card.component.scss' ]
})

export default class PostFeedCardComponent {
  @Input() post;
  @Input() displaySubreddit: boolean = true;

  @Output() onPostAction = new EventEmitter<object>();

  constructor( private postsService: PostsService ) { }

  get voteCount() {
    return abbreviate(this.post.score, 1, 1);
  }

  getPostThumbnail(): string {
    return this.post.preview
      ? this.post.preview.images[0].source.url
      : this.post.thumbnail;
  }

  canBePreviewed(): boolean {
    if (this.post.is_self && this.post.selftext_html)
      return true;
    else if (this.post.media !== null)
      return true;
    else if (this.post.preview && !isEmpty(this.post.preview.images[0].variants))
      return true;
    else if (this.post.post_hint && this.post.post_hint !== 'link')
      return true;
    else
      return false;
  }

  getRelativeDate(): string {
    return moment(this.post.created_utc * 1000).fromNow();
  }

  hasThumbnail(): boolean {
    return this.post.thumbnail.startsWith('http');
  }

  hasExternalLink(): boolean {
    return this.post.domain.indexOf('i.redd.it') >= 0 || this.post.is_self
      ? false
      : true;
  }

  votePost(dir: number): void {
    var previousState = this.post.likes;
    this.post.likes = dir === 0 ? null : numToBoolean(dir);

    this.postsService.vote(this.post.name, dir)
      .subscribe(() => {}, (err) => this.post.likes = previousState);
  }

  savePost(save:boolean = true): void {
    let previousState = this.post.saved;
    let saveAction = save ? 'saveThing' : 'unsaveThing';
    this.post.saved = save;

    this.postsService[saveAction](this.post.name)
      .subscribe(() => {}, (err) => this.post.saved = previousState);
  }

  emitPostAction(action: string): void {
    this.onPostAction.emit({ action, thing: this.post });
  }
}
