import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { PostsService } from 'services/posts-service/posts.service';
import { UserService } from 'services/user-service/user.service';

import { isEmpty } from 'underscore';

import { abbreviate, numToBoolean } from 'utils/utils';

const moment = require('moment');

@Component({
  selector: 'post-card',
  templateUrl: './post-card.component.html',
  styleUrls: [ './post-card.component.scss' ]
})

export default class PostCardComponent {
  @Input() post;
  @Output() onCommentSubmitted = new EventEmitter<object>();
  @Output() onPostAction = new EventEmitter<object>();

  private isCollapsed: boolean;

  constructor(
    private user: UserService,
    private sanitizer: DomSanitizer,
    private postsService: PostsService
  ) { }

  get postScore() {
    return abbreviate(this.post.score, 1, 1);
  }

  get relativeCreationDate() {
    return moment(this.post.created_utc * 1000).fromNow();
  }

  get selfText() {
    var txt = document.createElement('textarea');
    txt.innerHTML = this.post.selftext_html;
    return txt.value;
  }

  get postMedia() {
    var txt = document.createElement('textarea');
    txt.innerHTML = this.post.secure_media_embed.content;

    return this.sanitizer.bypassSecurityTrustHtml(txt.value);
  }

  // NOTE: This does not work with subreddits that disable previews
  get postImage() {
    return this.post.preview.images[0].variants.gif
      ? this.post.preview.images[0].variants.gif.source.url
      : this.post.preview.images[0].source.url;
  }

  get videoSource() {
    return this.post.media.reddit_video.fallback_url;
  }

  hasContent(): boolean {
    if (this.post.is_self && this.post.selftext_html)
      return true;
    else if (this.post.media !== null)
      return true;
    else if (this.post.preview && !isEmpty(this.post.preview.images[0].variants))
      return true;
    else if (this.post.post_hint && this.post.post_hint !== 'link')
      return true;
    else
      return false;
  }

  hasExternalLink(): boolean {
    return this.post.domain.indexOf('i.redd.it') >= 0 || this.post.is_self
      ? false
      : true;
  }

  votePost(dir: number): void {
    let previousState = this.post.likes;
    this.post.likes = dir === 0 ? null : numToBoolean(dir);

    this.postsService.vote(this.post.name, dir)
      .subscribe(() => {}, (err) => this.post.likes = previousState);
  }

  savePost(save:boolean = true): void {
    let previousState = this.post.saved;
    let saveAction = save ? 'saveThing' : 'unsaveThing';
    this.post.saved = save;

    this.postsService[saveAction](this.post.name)
      .subscribe(() => {}, (err) => this.post.saved = previousState);
  }

  emitPostAction(action: string): void {
    this.onPostAction.emit({ action, thing: this.post });
  }

  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }

  commentSubmited(comment): void {
    this.onCommentSubmitted.emit(comment)
  }
}
