import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';

import ProgressBar from 'progressbar.js';

@Component({
  selector: 'progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: [ './progress-bar.component.scss' ]
})

export default class ProgressBarComponent implements AfterViewInit {
  @ViewChild('barContainer') barContainer: ElementRef;

  @Input('type') _type: string;
  @Input() value: number;
  @Input() options = { };
  @Input() step: Function;

  private progressBar: any;

  get type(): string {
    return this._type.trim().replace(/^\b\w/, l => l.toUpperCase());
  }

  ngAfterViewInit() {

    let TypeConstructor = ProgressBar[this.type];
    let container = this.barContainer.nativeElement;
    let options = { ...this.options, step: this.step };

    this.progressBar = new TypeConstructor(container, options);

    this.progressBar.text.style.fontFamily = 'Lato, Helvetica, sans-serif';
    this.progressBar.text.style.fontSize = '2rem';

    if (this.type.toLowerCase() === 'circle') {
      this.progressBar.path.style.strokeLinecap = 'round'
      this.progressBar.path.style.filter = 'url(#dropshadow)';
    }

    this.progressBar.animate(this.value);
  }
}
