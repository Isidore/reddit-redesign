import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'burger-button',
  templateUrl: './burger-button.component.html',
  styleUrls: [ './burger-button.component.scss' ]
})

export default class BurgerButtonComponent {
  @Input() isActive: boolean = false;
}
