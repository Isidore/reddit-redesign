import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';

import { SubredditService } from 'services/subreddit-service/subreddit.service';

@Component({
  selector: 'create-sub-card',
  templateUrl: './create-sub-card.component.html',
  styleUrls: [ './create-sub-card.component.scss' ]
})

export default class CreateSubCardComponent {
  private subForm: FormGroup;
  private validSubName: boolean = true;
  private loadingRequest: boolean = false;
  private validationErrors: any[] = [];
  private submissionSuccess: boolean = false;

  @Output() onCancel = new EventEmitter<void>();

  constructor(
    private subService: SubredditService,
    private fb: FormBuilder
  ) {
    this.createForm();
    this.validateName();
  }

  // TODO: Maybe save the form's initial model in a variable?
  createForm() {
    this.subForm = this.fb.group({
      name: ['', Validators.required],
      title: ['', Validators.required],
      description: '',
      public_description: '',
      type: ['public', Validators.required],
      link_type: ['any', Validators.required],
      wikimode: ['disabled', Validators.required]
    });
  }

  resetForm() {
    this.subForm.reset({
      name: '',
      title: '',
      description: '',
      public_description: '',
      type: 'public',
      link_type: 'any',
      wikimode: 'disabled'
    });
  }

  validateName() {
    const nameControl = this.subForm.get('name');
    nameControl.valueChanges
      .distinctUntilChanged()
      .debounceTime(300)
      .subscribe((subName) => {
        this.subService.searchSubNames(subName, true).subscribe(
          (names) => { this.validSubName = names.length > 0 ? false : true },
          (err) => { if (err.status === 404) this.validSubName = true })
      });
  }

  submitData() {
    this.loadingRequest = true;
    this.validationErrors = [];
    let subData = this.subForm.value;

    this.subForm.disable();

    console.log("subData: ", subData);

    this.subService.createSub(subData).subscribe((data) => {
      this.subForm.enable();
      this.loadingRequest = false;
      console.log("Success response: ", data);

      if (data.errors.length > 0) {
        data.errors.forEach((error) => {
          this.validationErrors
              .push({ code: error[0], reason: error[1], field: error[2] });
        });
      } else {
        this.resetForm();
        this.submissionSuccess = true;
      }
    });
  }

  emitCancel() {
    this.onCancel.emit();
  }
}
