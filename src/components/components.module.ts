import { NgModule }                         from '@angular/core';
import { CommonModule }                     from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule }                     from '@angular/router';
import { BrowserAnimationsModule }          from '@angular/platform-browser/animations';

import { TagInputModule }                   from 'ngx-chips';
import { TruncateModule }                   from 'ng2-truncate';
import { InfiniteScrollModule }             from 'ngx-infinite-scroll';

//  I'm using this method so we don't have to repeat this list
//  twice.
const components =
[ require('./sub-info-feed-card').default
, require('./create-sub-card').default
, require('./floating-button').default
, require('./comment-editor').default
, require('./share-post-card').default
, require('./burger-button').default
, require('./post-feed-card').default
, require('./submit-button').default
, require('./score-button').default
, require('./progress-bar').default
, require('./post-preview').default
, require('./comment-card').default
, require('./split-button').default
, require('./report-card').default
, require('./post-submit').default
, require('./select-box').default
, require('./search-bar').default
, require('./post-feed').default
, require('./post-card').default
];

@NgModule({
  imports: [
    RouterModule.forChild([]),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,

    InfiniteScrollModule,
    TruncateModule,
    TagInputModule
  ],

  declarations: components,
  exports: components
})

export class ComponentsModule { }
