import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';

import { map } from 'underscore';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/distinctUntilChanged';

import { MultiredditService } from 'services/multireddit-service/multireddit.service';
import { SubredditService } from 'services/subreddit-service/subreddit.service';
import { PostsService } from 'services/posts-service/posts.service';
import { FeedService }  from 'services/feed-service/feed.service';

import { togglePageScroll } from 'utils/utils';

@Component({
  selector: 'post-feed',
  templateUrl: './post-feed.component.html',
  styleUrls: [ './post-feed.component.scss' ]
})

export default class PostFeedComponent implements OnInit {
  private postFeed: object[] = [];
  private subFeed: object[] = [];
  private overlayPost: object;
  private lastPost: string = '';

  private showOverlay: boolean = false;
  private overlayData
    : { action: string, thing: object }
    = { action: '', thing: {} };

  constructor(
    private multiService: MultiredditService,
    private subService: SubredditService,
    private postsService: PostsService,
    private feedService: FeedService
  ) { }

  ngOnInit() {
    this.feedService.sortMode$
      .combineLatest(this.feedService.feedSource$)
      .combineLatest(this.feedService.searchQuery$)
      .distinctUntilChanged()
      .debounceTime(300)
      .subscribe((filters) => {
        return filters[1].trim() ? this.search(true) : this.fetchPosts(true);
      });
  }

  fetchPosts(overrideFeed = false): void {
    var sort = this.feedService.sortMode;
    var src = this.feedService.feedSource;
    this.lastPost = overrideFeed ? '' : this.lastPost;

    switch(true) {
      case src.startsWith('r/'):
        this.postsService
          .getPosts(sort, src.replace('r/', ''), this.lastPost)
          .subscribe(this.handleFeedData.bind(this, overrideFeed));
        break;

      // This one is more complex so we need a RegExp
      case /^\/user\/[\w\-]+\/m\/\w+\/$/.test(src):
        this.multiService
          .getMultiFeed(src, sort, this.lastPost)
          .subscribe(this.handleFeedData.bind(this, overrideFeed));
        break;

      case src === 'home':
        this.postsService
          .getPosts(sort, '', this.lastPost)
          .subscribe(this.handleFeedData.bind(this, overrideFeed));
        break;

      case src === 'saved':
        this.postsService
          .getSavedPosts(sort, this.lastPost)
          .subscribe(this.handleFeedData.bind(this, overrideFeed));
        break;
    }
  }

  search(overrideFeed = false): void {
    if (this.feedService.feedSource.startsWith('sr:'))
      this.searchSubreddits(overrideFeed);
    else
      this.searchPosts(overrideFeed);
  }

  // TODO: This absolutely needs to include saved posts
  // and Multireddit posts
  searchPosts(overrideFeed = false): void {
    var searchQuery = this.feedService.searchQuery;
    var sort = this.feedService.sortMode;
    var src = this.feedService.feedSource;
    this.lastPost = overrideFeed ? '' : this.lastPost;

    if (src.startsWith('r/')) {
      this.postsService
        .search(searchQuery, sort, src.replace('r/', ''), this.lastPost)
        .subscribe(this.handleFeedData.bind(this, overrideFeed));
    } else if (/^\/user\/[\w\-]+\/m\/\w+\/$/.test(src)) {
      this.multiService
        .searchMultiFeed(src, searchQuery, sort, this.lastPost)
        .subscribe(this.handleFeedData.bind(this, overrideFeed));
    } else {
      this.postsService
        .search(searchQuery, sort, '', this.lastPost)
        .subscribe(this.handleFeedData.bind(this, overrideFeed));
    }
  }

  searchSubreddits(overrideFeed = false): void {
    let searchQuery = this.feedService.searchQuery;
    let sort = this.feedService.sortMode;
    this.lastPost = overrideFeed ? '' : this.lastPost;

    this.subService
      .searchSubs(searchQuery, sort, this.lastPost)
      .subscribe(this.handleFeedData.bind(this, overrideFeed));
  }

  handleFeedData(override: boolean, data: any): void {
    let source = this.feedService.feedSource;
    let dataFeed = source.startsWith('sr:') ? 'subFeed' : 'postFeed';

    //  TODO: Fix that ASAP
    this[dataFeed === 'postFeed' ? 'subFeed' : 'postFeed'] = [];

    this.lastPost = data.after;
    this[dataFeed] = override
      ? data.children
      : this[dataFeed].concat(data.children);
  }

  openOverlay(overlayData): void {
    togglePageScroll(false);
    this.showOverlay = true;
    this.overlayData = overlayData;
  }

  closeOverlay(): void {
    togglePageScroll(true);
    this.showOverlay = false;
    this.overlayData = { action: '', thing: {} };
  }

  onScroll(): void {
    if (this.feedService.searchQuery.trim())
      this.search();
    else
      this.fetchPosts();
  }
}
