import { Component, Input, Output, EventEmitter } from '@angular/core';

import { UserService } from 'services/user-service/user.service';
import { PostsService } from 'services/posts-service/posts.service';

import { abbreviate, numToBoolean } from 'utils/utils';

const moment = require('moment');

@Component({
  selector: 'comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: [ './comment-card.component.scss' ]
})

export default class CommentCardComponent {
  @Input() comment;
  @Input() isParent:boolean;

  @Output() onCommentAction = new EventEmitter<object>();

  private isCollapsed:boolean = false;
  private showReplyEditor:boolean = false;

  constructor(
    private user: UserService,
    private postsService: PostsService
  ) { }

  get commentBody() {
    var txt = document.createElement('textarea');
    txt.innerHTML = this.comment.body_html;
    return txt.value;
  }

  commentScore(): string {
    return abbreviate(this.comment.score, 1, 1);
  }

  relativeCreationDate(): string {
    return moment(this.comment.created_utc * 1000).fromNow();
  }

  voteComment(dir): void {
    let previousState = this.comment.likes;
    this.comment.likes = dir === 0 ? null : numToBoolean(dir);

    this.postsService.vote(this.comment.name, dir)
      .subscribe(() => {}, (err) => this.comment.likes = previousState);
  }

  saveComment(isSaved:boolean = true): void {
    let previousState = this.comment.saved;
    let saveAction = isSaved ? 'saveThing' : 'unsaveThing';
    this.comment.saved = isSaved;

    this.postsService[saveAction](this.comment.name)
    .subscribe(() => {}, (err) => this.comment.saved = previousState);
  }

  reportComment(): void {
    this.onCommentAction.emit({ action: 'report', thing: this.comment });
  }

  collapseComment(ev): void {
    ev.preventDefault();
    this.isCollapsed = !this.isCollapsed;
  }

  toggleReplyEditor(ev?): void {
    ev ? ev.preventDefault() : undefined;
    this.showReplyEditor = !this.showReplyEditor;
  }

  // TODO: Refactor that
  addChild(childComment): void {
    if (typeof this.comment.replies === 'string')
      this.comment.replies = { data: { children: [] } };

    this.comment.replies.data.children.unshift(childComment);
    this.showReplyEditor = false;
  }
}
