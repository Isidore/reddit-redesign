import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef
} from '@angular/core';

import { where } from 'underscore';

@Component({
  selector: 'select-box',
  templateUrl: './select-box.component.html',
  styleUrls: [ './select-box.component.scss' ]
})

export default class SelectBoxComponent implements OnInit {
  private isActive = false;
  private activeOption:any = { };

  @Input() options: Array<Object>;

  private _value: string;
  @Output() valueChange = new EventEmitter<any>();

  constructor( private cdr: ChangeDetectorRef ) { }

  ngOnInit() {
    this.activeOption = where(this.options, { selected: true })[0] || this.options[0];

    // TODO: This is hacky, fix it if possible.
    if (this.activeOption.selected)
      setTimeout(() => this.value = this.activeOption.value, 1);
  }

  @Input()
  get value() {
    return this._value;
  }

  set value(value: string) {
    this._value = value;
    this.valueChange.emit(this._value);
  }

  selectOption(option): void {
    this.activeOption = option;
    this.value = option.value;
  }

  toggleOptions(): void {
    this.isActive = !this.isActive;
  }
}
