import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { UserService } from 'services/user-service/user.service';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const apiConfig = require('config/authConfig.json');

@Injectable()
export class MultiredditService {
  private apiRootURL: string;
  private reqOptions = new RequestOptions({});

  private addedMulti  = new Subject<any>();
  public  addedMulti$ = this.addedMulti.asObservable();

  private removedMulti  = new Subject<any>();
  public  removedMulti$ = this.removedMulti.asObservable();

  private changedMulti  = new Subject<any>();
  public  changedMulti$ = this.changedMulti.asObservable();

  constructor(private http: Http, private user: UserService) {
    this.reqOptions.headers = user.getAuthenticatedHeaders();
    this.apiRootURL = user.isAuthenticated()
      ? apiConfig.authBaseURI
      : apiConfig.baseURI;
  }

  getMultiFeed(multiPath:string, sort='', after='', limit=15): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { after, limit } });
    let userName = multiPath.match(/^\/user\/([\w\-]+)\/m\/\w+\/$/)[1];
    let multiName = multiPath.match(/^\/user\/[\w\-]+\/m\/(\w+)\/$/)[1];

    let feedURI = userName === this.user.userData.name
      ? `${this.apiRootURL}/me/m/${multiName}/${sort}/.json`
      : `${this.apiRootURL}/user/${userName}/m/${multiName}/${sort}/.json`;

    return this.http
      .get(feedURI, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  searchMultiFeed(multiPath:string, q:string, sort = 'relevance', after='', limit='15'): Observable<any> {
    let search = { q, sort, after, limit, include_over_18: true, restrict_sr: true };
    let userName = multiPath.match(/^\/user\/([\w\-]+)\/m\/\w+\/$/)[1];
    let multiName = multiPath.match(/^\/user\/[\w\-]+\/m\/(\w+)\/$/)[1];

    let feedURI = userName === this.user.userData.name
      ? `${this.apiRootURL}/me/m/${multiName}/search.json`
      : `${this.apiRootURL}/user/${userName}/m/${multiName}/search.json`;

    return this.http
      .get(feedURI, this.reqOptions.merge({ search }))
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  getUserMultis(expand_srs = false): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { expand_srs } });

    return this.http
      .get(`${this.apiRootURL}/api/multi/mine`, reqOptions)
      .map(res => res.json())
      .catch(this.handleError);
  }

  getMultiData(multiName:string, userName:string, expand_srs = false): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { expand_srs } });

    return this.http
      .get(`${this.apiRootURL}/api/multi/user/${userName}/m/${multiName}`, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  copyMulti(from:string, display_name:string): Observable<any> {
    let to = from.replace(/(m\/)(\w+)\/$/, `$1${display_name}/`);
    let reqOptions = this.reqOptions.merge({ search: { from, to, display_name } });

    return this.http
      .post(`${this.apiRootURL}/api/multi/copy`, { }, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  createMulti(model): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { model } });

    return this.http
      .post(`${this.apiRootURL}/api/multi`, { }, reqOptions)
      .map(res => res.json())
  }

  updateMulti(multipath:string, model): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { model } });

    return this.http
      .put(`${this.apiRootURL}/api/multi/${multipath}`, { }, reqOptions)
      .map(res => res.json())
      .catch(this.handleError);
  }

  deleteMulti(multipath:string): Observable<any> {
    return this.http
      .delete(`${this.apiRootURL}/api/multi${multipath}`, this.reqOptions)
      .catch(this.handleError);
  }

  renameMulti(from:string, display_name:string): Observable<any> {
    let to = from.replace(/(m\/)(\w+)\/$/, `$1${display_name}/`);
    let reqOptions = this.reqOptions.merge({ search: { from, to, display_name } });

    return this.http
      .post(`${this.apiRootURL}/api/multi/rename`, { }, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError)
  }

  removeFromMulti(multipath:string, srname:string): Observable<any> {
    return this.http
      .delete(`${this.apiRootURL}/api/multi/${multipath}/r/${srname}`, this.reqOptions)
      .catch(this.handleError);
  }

  addToMulti(multipath, model): Observable<any> {
    return this.http
      .put(`${this.apiRootURL}/api/multi/${multipath}/r/${model.name}`, {})
      .map(res => res.json())
      .catch(this.handleError);
  }

  updateDescription(multipath, body_md): Observable<any> {
    let model = { body_md };
    let reqOptions = this.reqOptions.merge({ search: { model } });

    return this.http
      .put(`${this.apiRootURL}/api/multi${multipath}description`, { }, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  emitAddedMulti(multi: any) {
    this.addedMulti.next(multi);
  }

  emitRemovedMulti(multi: any) {
    this.removedMulti.next(multi);
  }

  emitChangedMulti(from: any, to: any) {
    this.changedMulti.next({ from, to });
  }

  private handleError(error: Response | any): Observable<any> {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
