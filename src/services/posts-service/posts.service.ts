import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { UserService } from 'services/user-service/user.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { each, where, findWhere, findIndex, filter } from 'underscore';

const apiConfig = require('config/authConfig.json');

@Injectable()
export class PostsService {
  private apiRootURL: string;
  private reqOptions = new RequestOptions({});

  constructor( private http: Http, private userService: UserService ) {
    this.reqOptions.headers = userService.getAuthenticatedHeaders();
    this.apiRootURL = userService.isAuthenticated()
      ? apiConfig.authBaseURI
      : apiConfig.baseURI;
  }

  getPosts(sort = 'hot', sr = '', after = '', limit = 15): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { after, limit } });

    let postsUri = sr
      ? `${this.apiRootURL}/r/${sr}/${sort}/.json`
      : `${this.apiRootURL}/${sort}/.json`;

    return this.http
      .get(postsUri, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError)
  }

  getSavedPosts(sort = 'hot', after = '', limit = 15, type = 'link'): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { after, limit, type } });
    let userName = this.userService.userData.name;

    return this.http
      .get(`${this.apiRootURL}/user/${userName}/saved`, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  //  TODO: Rename to searchPosts
  search(term: string, sort = 'relevance', sr = '', after = '', limit = '15'): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { sort, after, limit } });

    let postsUri = sr
      ? `${this.apiRootURL}/r/${sr}/search.json?q=${term}&restrict_sr=on`
      : `${this.apiRootURL}/search.json?q=${term}`;

    return this.http
      .get(postsUri, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  // TODO: Make sure the sorting mode works
  getPostComments(sr: string, postId: string, options = {}): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: options });

    return this.http
      .get(`${this.apiRootURL}/r/${sr}/comments/${postId}/.json`, reqOptions)
      .map(res => ({ post: res.json()[0].data.children[0].data, comments: res.json()[1].data.children }))
      .catch(this.handleError)
  }

  getMoreComments(postId:string, children:string[], sort:string, limit_children = false): Observable<any> {
    let reqOptions = this.reqOptions
      .merge({ search:
        { link_id: postId
        , children: children.join(',')
        , api_type: 'json'
        , sort
        , limit_children
        }
      });

    return this.http
      .get(`${this.apiRootURL}/api/morechildren.json`, reqOptions)
      .map(this.unflattenPostComments)
      .catch(this.handleError);
  }

  submitPost(post): Observable<any> {
    let reqOptions = this.reqOptions.merge({
      search: { ...post, api_type: 'json' }
    });

    return this.http
      .post(`${this.apiRootURL}/api/submit`, { }, reqOptions)
      .map(res => res.json().json)
      .catch(this.handleError);
  }

  submitComment(comment): Observable<any> {
    let reqOptions = this.reqOptions.merge({
      search: { ...comment, api_type: 'json' }
    });

    return this.http
      .post(`${this.apiRootURL}/api/comment`, { }, reqOptions)
      .map(res => res.json().json)
      .catch(this.handleError);
  }

  saveThing(id: string): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { id } });

    return this.http
      .post(`${this.apiRootURL}/api/save`, {}, reqOptions)
      .catch(this.handleError);
  }

  unsaveThing(id: string): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { id } });

    return this.http
      .post(`${this.apiRootURL}/api/unsave/`, {}, reqOptions)
      .catch(this.handleError);
  }

  vote(id: string, dir: number): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { id, dir } });

    return this.http
      .post(`${this.apiRootURL}/api/vote`, {}, reqOptions)
      .catch(this.handleError);
  }

  reportThing(thing_id: string, reason: object): Observable<any> {
    let reqOptions = this.reqOptions.merge({
      search: { ...reason, thing_id, api_type: 'json' }
    });

    return this.http
      .post(`${this.apiRootURL}/api/report`, {}, reqOptions)
      .map(res => res.json().json)
      .catch(this.handleError);
  }

  private handleError(error: Response | any): Observable<any> {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private unflattenPostComments(res: Response): any {
    console.log("moreChildren: ", res.json().json);
    var resData = res.json().json;

    var mapReplies = (comment, i, comments) => {
      if (i >= comments.length) return;
      let children = filter(comments, (c) => c.data.parent_id === comment.data.name);

      each(children, (child) => {
        if (comment.kind !== 'more')
          mapReplies(child, findIndex(comments, child), comments);

        comment.data.replies.data
          ? comments[i].data.replies.data.children.push(child)
          : comments[i].data.replies = { data: { children: [child] } }

        comments.splice(findIndex(comments, child), 1);
      });
    }

    return {
      comments: where(each(resData.data.things, mapReplies), { kind: 't1' }),
      moreChildren: findWhere(resData.data.things, { kind: 'more' }) || {}
    }
  }
}
