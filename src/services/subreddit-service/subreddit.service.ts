import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { UserService } from 'services/user-service/user.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as _ from 'underscore';

const apiConfig = require('config/authConfig.json');

@Injectable()
export class SubredditService {
  private apiRootURL: string;
  private reqOptions = new RequestOptions({});

  constructor(private http: Http, private userService: UserService) {
    this.reqOptions.headers = userService.getAuthenticatedHeaders();
    this.apiRootURL = userService.isAuthenticated()
      ? apiConfig.authBaseURI
      : apiConfig.baseURI;
  }

  createSub(subData: any): Observable<any> {
    let reqOptions = this.reqOptions
      .merge({ search: { ...subData, api_type: 'json' } });

    return this.http
      .post(`${this.apiRootURL}/api/site_admin`, { }, reqOptions)
      .map(res => res.json().json)
      .catch(this.handleError);
  }

  getSubInfo(srName: string): Observable<any> {
    return this.http
      .get(`${this.apiRootURL}/r/${srName}/about.json`, this.reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  getSubRules(srName: string): Observable<any> {
    return this.http
      .get(`${this.apiRootURL}/r/${srName}/about/rules.json`, this.reqOptions)
      .map(res => res.json())
      .catch(this.handleError);
  }

  getSubMods(srName: string): Observable<any> {
    return this.http
      .get(`${this.apiRootURL}/r/${srName}/about/moderators.json`, this.reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  getSubStyle(srName: string): Observable<any> {
    return this.http
      .get(`${apiConfig.baseURI}/r/${srName}/about/stylesheet.json`)
      .map(this.parseSubStyle)
      .catch(this.handleError);
  }

  setUserFlair(srName: string, flair_enabled: boolean): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { flair_enabled, api_type: 'json' } });

    return this.http
      .post(`${this.apiRootURL}/r/${srName}/api/setflairenabled`, {}, reqOptions)
      .map(res => res.json().json)
      .catch(this.handleError);
  }

  searchSubs(term:string, sort = 'relevance', after = '', limit = '15'): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { sort, after, limit } });

    return this.http
      .get(`${this.apiRootURL}/search.json?q=${term}&type=sr`, reqOptions)
      .map(res => res.json().data)
      .catch(this.handleError);
  }

  searchSubNames(query: string, exact = false, include_over_18 = true): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { query, exact, include_over_18 } });

    // TODO: Please fix that
    let body = JSON.stringify({ });

    return this.http
      .post(`${this.apiRootURL}/api/search_reddit_names.json`, body, reqOptions)
      .map(res => res.json().names)
      .catch(this.handleError);
  }

  getUserSubs(where = 'subscriber', after: string, limit = '25'): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { after, limit } });

    return this.http
      .get(`${this.apiRootURL}/subreddits/mine/${where}/.json`, reqOptions)
      .map(this.mapSubListing)
      .catch(this.handleError);
  }

  getSubs(where = 'popular', after: string, limit = '25'): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { after, limit } });

    return this.http
      .get(`${this.apiRootURL}/subreddits/${where}/.json`, reqOptions)
      .map(this.mapSubListing)
      .catch(this.handleError);
  }

  subscribeUser(sr: string, action: string): Observable<any> {
    let reqOptions = this.reqOptions.merge({ search: { sr, action } });

    return this.http
      .post(`${this.apiRootURL}/api/subscribe`, {}, reqOptions)
      .map(res => res.json())
      .catch(this.handleError);
  }

  private parseSubStyle(res: Response): string {
    var stylesheet = res.json().data.stylesheet;
    var images = res.json().data.images;

    let re = new RegExp("%{2}(?![()])([A-Za-z0-9_\-]+)%{2}", "g");

    return stylesheet.replace(re, (match, imgName) => {
      let imgUrl = '';

      images.forEach((image) => {
        if (image.name === imgName) imgUrl = image.url;
      });

      return imgUrl;
    });
  }

  private mapSubListing(res: Response): string {
    return _.tap(res.json().data, (data) => {
      data.children = _.map(data.children, (sub) => sub.data);
    });
  }

  private handleError(error: Response | any): Observable<any> {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);
    return Observable.throw(error);
  }
}
