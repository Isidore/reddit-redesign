import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { OAuthService } from 'angular-oauth2-oidc';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

const oauthConfig = require('config/authConfig.json');

// TODO: Create a proper events API

@Injectable()
export class UserService {
  private _userData: any;

  private loadingUser = new BehaviorSubject<boolean>(false);
  public  loadingUser$ = this.loadingUser.asObservable();

  private tokenExpired = new BehaviorSubject<boolean>(false);
  public  tokenExpired$ = this.tokenExpired.asObservable();

  constructor( private http: Http, private oAuthService: OAuthService ) { }

  setup(): void {
    this.oAuthService.loginUrl = oauthConfig.loginUrl;
    this.oAuthService.logoutUrl = oauthConfig.logoutUrl;
    this.oAuthService.redirectUri = oauthConfig.redirectUri;
    this.oAuthService.clientId = oauthConfig.clientId;
    this.oAuthService.scope = oauthConfig.scope.join(',');
    this.oAuthService.requireHttps = oauthConfig.requireHttps;

    this.oAuthService.oidc = oauthConfig.enableOidc;
    this.oAuthService.setStorage(localStorage);

    this.oAuthService.tryLogin({
      onTokenReceived: this.getIdentity.bind(this)
    });

    if (this.isAuthenticated() && !this._userData) {
      this.loadingUser.next(true);
      this.getIdentity();
    }
  }

  getIdentity(): void {
    let options = new RequestOptions({ headers: this.getAuthenticatedHeaders() });
    let expiresAt = JSON.parse(localStorage.getItem('expires_at'));

    setTimeout(() => this.tokenExpired.next(true), expiresAt - Date.now());

    this.http
      .get(`${oauthConfig.authBaseURI}/api/v1/me.json`, options)
      .subscribe((res) => {
        this._userData = res.json()
        this.loadingUser.next(false);
      });
  }

  login(): void {
    this.oAuthService.initImplicitFlow();
  }

  logout(): void {
    this.oAuthService.logOut();
  }

  // NOTE: Try loggin in into your account from two different
  // browsers, have fun.
  isAuthenticated(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  getToken(): string {
    return this.oAuthService.getAccessToken();
  }

  getAuthenticatedHeaders(): Headers {
    let accessToken = this.oAuthService.getAccessToken();

    return this.isAuthenticated()
      ? new Headers({ 'Authorization': `Bearer ${accessToken}` })
      : new Headers({ });
  }

  get userData() {
    return this._userData || { };
  }
}
