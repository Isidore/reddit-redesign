import { Component, Input, OnInit } from '@angular/core';

import { MultiredditService } from 'services/multireddit-service/multireddit.service';
import { SubredditService } from 'services/subreddit-service/subreddit.service';
import { UserService } from 'services/user-service/user.service';

import { map, sortBy, reject, findIndex } from 'underscore';

@Component({
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: [ 'sidebar.component.scss' ]
})

export default class SidebarComponent implements OnInit {
  @Input() isActive: boolean;

  private multiredditList: Array<any> = [];
  private loadingMultiList: boolean = false;

  private subredditList: Array<any> = [];
  private lastSubItem: string = '';
  private loadingSubList: boolean;

  constructor(
    private multiService: MultiredditService,
    private subService: SubredditService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.fetchMultiList();
    this.fetchSubList();

    this.multiService.addedMulti$
      .subscribe((multi) => {
        this.multiredditList = sortBy([...this.multiredditList, multi], 'name');
        console.log("multiService.addedMulti: ", multi)
        console.log("Sidebar.multiList: ", this.multiredditList)
      });

    this.multiService.removedMulti$
      .subscribe(({ path }) => {
        this.multiredditList = sortBy(reject(this.multiredditList, { path }), 'name');
        console.log("multiService.removedMulti: ", path);
        console.log("Sidebar.multiList: ", this.multiredditList)
      });

    this.multiService.changedMulti$
      .subscribe(({ from, to }) => {
        let changedMulti = findIndex(this.multiredditList, { path: from.path });
        this.multiredditList[changedMulti] = to;

        console.log("multiService.changedMulti: ", { from, to });
      });
  }

  // NOTE: Executing this method during initialization can cause a
  // potential problem for the subList pagination, since the layout
  // as a whole might get bigger when the multiList is fetched, and
  // that usually happens pretty quickly.
  fetchMultiList(): void {
    this.loadingMultiList = true;

    if (this.userService.isAuthenticated()) {
      this.multiService.getUserMultis()
        .subscribe((multis) => this.multiredditList = map(multis, m => m.data));
    }
  }

  fetchSubList(overrideSubList = false): void {
    // We've reached the end of the listing
    if (this.lastSubItem === null) return;

    this.loadingSubList = true;
    this.lastSubItem = overrideSubList ? '' : this.lastSubItem;

    if (this.userService.isAuthenticated()) {
      this.subService.getUserSubs('subscriber', this.lastSubItem)
        .subscribe(this.handleSubData.bind(this, overrideSubList));
    } else {
      this.subService.getSubs('popular', this.lastSubItem)
        .subscribe(this.handleSubData.bind(this, overrideSubList));
    }
  }

  handleSubData(override: boolean, data: any): void {
    this.lastSubItem = data.after;
    this.subredditList = override
      ? data.children
      : this.subredditList.concat(data.children);
    this.loadingSubList = false;
  }

  onScroll() {
    console.log("Sidebar Scroll!");
    this.fetchSubList();
  }
}
