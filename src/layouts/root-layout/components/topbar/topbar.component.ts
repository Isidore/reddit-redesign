import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { UserService } from 'services/user-service/user.service';

@Component({
  selector: 'app-topbar',
  templateUrl: 'topbar.component.html',
  styleUrls: [ 'topbar.component.scss' ]
})

export default class TopbarComponent implements OnInit {
  private pageLabel = 'FRONTPAGE';
  private isSubreddit:boolean = false;

  @Input() isSidebarActive: boolean;
  @Output() onSidebarToggle = new EventEmitter<boolean>();

  constructor(
    private route: ActivatedRoute,
    private user: UserService,
    private router: Router
  ) { }

  //  TODO: This event will not fire when the user first loads the page
  //  it only fires when you navigate from one page of the application
  //  to another one. This is because of the event listeners on the root
  //  layout that are delaying it
  ngOnInit() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.route)
      .map((route) => {
        while (route.firstChild && !route.snapshot.params.subredditName)
          route = route.firstChild;
        return route;
      })
      .filter(route => route.outlet === 'primary')
      .subscribe(({ snapshot }) => {
        this.pageLabel = snapshot.params.subredditName
          ? `r/${snapshot.params.subredditName.toUpperCase()}`
          : 'FRONTPAGE';
      });
  }

  toggleSidebar(): void {
    this.onSidebarToggle.emit(!this.isSidebarActive);
  }

  login(): void {
    this.user.login();
  }

  logout(): void {
    this.user.logout();
    window.location.reload();
  }
}
