import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { UserService } from 'services/user-service/user.service';

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Component({
  selector: 'root-layout',
  templateUrl: './root.layout.html'
})

export default class RootLayout implements OnInit {
  private showSidebar = false;
  private loadingUser = true;

  constructor(
    private title: Title,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private user: UserService
  ) { }

  ngOnInit() {
    this.router.events
      .filter(ev => ev instanceof NavigationEnd)
      .do(() => window.scrollTo(0, 0))
      .map(() => this.activatedRoute)
      .map(rt => { while (rt.firstChild) rt = rt.firstChild; return rt; })
      .filter(route => route.outlet === 'primary')
      .mergeMap(route => route.data)
      .subscribe(ev => this.title.setTitle(ev['title']));

    this.user.loadingUser$
      .subscribe((isLoading) => this.loadingUser = isLoading);
    this.user.tokenExpired$
      .subscribe((isExpired) => { if (isExpired) window.location.reload() });
  }

  toggleSidebar(): void {
    this.showSidebar = !this.showSidebar;
  }
}
