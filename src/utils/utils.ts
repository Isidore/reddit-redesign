//  I had to write this function because Boolean(-1) will return true,
//  and while this is correct, this is not always what you want.
export function numToBoolean(num) {
  return num > 0 ? true : false;
}

//  For details of the implementation refer to:
//  https://stackoverflow.com/questions/4068373/center-a-popup-window-on-screen
export function popupWindow(url:string, w:number, h:number, title?:string): void {
  var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : (<any>screen).left;
  var dualScreenTop = window.screenTop !== undefined ? window.screenTop : (<any>screen).top;

  var width = window.innerWidth
    ? window.innerWidth
    : document.documentElement.clientWidth
        ? document.documentElement.clientWidth
        : screen.width;

  var height = window.innerHeight
    ? window.innerHeight
    : document.documentElement.clientHeight
        ? document.documentElement.clientHeight
        : screen.height;

  var left = ((width / 2) - (w / 2)) + dualScreenLeft;
  var top = ((height / 2) - (h / 2)) + dualScreenTop;

  var newWindow = window.open(
    url,
    title,
    `scrollbars=yes, width=${w}, height=${h}, top=${top}, left=${left}`
  );

  if (window.focus)
    newWindow.focus();
}

export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function abbreviate(num, maxPlaces?, forcePlaces?, forceLetter?) {
  var abbr = '';

  num = Number(num);
  forceLetter = forceLetter || false

  if (forceLetter !== false) {
    return annotate(num, maxPlaces, forcePlaces, forceLetter);
  }


  if (num >= 1e12) abbr = 'T'
  else if (num >= 1e9) abbr = 'B'
  else if (num >= 1e6) abbr = 'M'
  else if (num >= 1e3) abbr = 'K'

  return annotate(num, maxPlaces, forcePlaces, abbr);
}

function annotate(num, maxPlaces?, forcePlaces?, abbr?) {
  var rounded: any = 0;

  switch(abbr) {
    case 'T':
      rounded = num / 1e12;
      break;
    case 'B':
      rounded = num / 1e9;
      break;
    case 'M':
      rounded = num / 1e6;
      break;
    case 'K':
      rounded = num / 1e3;
      break;
    default:
      rounded = num;
      break;
  }

  if (maxPlaces !== false) {
    var test = new RegExp('\\.\\d{' + (maxPlaces + 1) + ',}$');

    if (test.test(('' + rounded))) rounded = rounded.toFixed(maxPlaces);
  }

  if ((forcePlaces !== false) && !(num < 1e3)) {
    rounded = Number(rounded).toFixed(forcePlaces);
  }

  return rounded + abbr
}

export function togglePageScroll(scrollState: boolean) {
  if (!scrollState) {
    if(document.body.className.indexOf('no-scroll') === -1)
      document.body.className += ' ' + 'no-scroll'
  } else {
    document.body.className = document.body.className.replace('no-scroll', '');
  }
}

// This function was directly taken and modified from the
// https://github.com/thomaspark/scoper repository
export function scoper(css, prefix) {
  var re = new RegExp("([^\r\n,{}]+)(,(?=[^}]*{)|\s*{)", "g");
  css = css.replace(re, function(g0, g1, g2) {

    if (g1.match(/^\s*(@media|@.*keyframes|to|from|@font-face|1?[0-9]?[0-9])/)) {
      return g1 + g2;
    }

    if (g1.match(/:scope/)) {
      g1 = g1.replace(/([^\s]*):scope/, function(h0, h1) {
        if (h1 === "") {
          return "> *";
        } else {
          return "> " + h1;
        }
      });
    }

    g1 = g1.replace(/^(\s*)/, "$1" + prefix + " ");

    return g1 + g2;
  });

  return css;
}
