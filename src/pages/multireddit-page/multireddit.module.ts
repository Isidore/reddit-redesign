import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TagInputModule } from 'ngx-chips';
import { ComponentsModule } from 'components/components.module';

import MultiredditPage from './multireddit.page';

import PostFeedFiltersComponent from './components/post-feed-filters';

import MultiredditSidebarComponent from './components/multireddit-sidebar';
import CreateMultiCardComponent from './components/create-multi-card';
import MultiInfoPanelComponent from './components/multi-info-panel';
import SubListPanelComponent from './components/sub-list-panel';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    ComponentsModule,
    TagInputModule
  ],

  declarations: [
    MultiredditPage,

    PostFeedFiltersComponent,

    MultiredditSidebarComponent,
    CreateMultiCardComponent,
    MultiInfoPanelComponent,
    SubListPanelComponent
  ],

  exports: [ MultiredditPage ]
})

export class MultiredditPageModule { }
