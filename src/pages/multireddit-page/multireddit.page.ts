import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MultiredditService } from 'services/multireddit-service/multireddit.service';
import { FeedService } from 'services/feed-service/feed.service';

import { ISubscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/skip';

@Component({
  templateUrl: './multireddit.page.html',
  providers: [ FeedService ]
})

export default class MultiredditPage implements OnInit, OnDestroy {
  private userOwnsMulti: boolean;
  private multiName: string;
  private userName: string;

  private multiInfo: any;

  private routerSubscription: ISubscription;

  constructor(
    private multiService: MultiredditService,
    private feedService: FeedService,

    private route: ActivatedRoute,
    private title: Title,
    private router: Router
  ) {
    this.routerSubscription = router.events
      .skip(1)
      .filter(ev => ev instanceof NavigationEnd)
      .subscribe((ev: NavigationEnd) => {
        if (/^\/user\/[\w\-]+\/m\/\w+$/.test(ev.url))
          this.ngOnInit();
      });
  }

  ngOnInit() {
    this.multiName = this.route.snapshot.params['multiName'];
    this.userName = this.route.snapshot.params['userName']
    this.feedSource = `/user/${this.userName}/m/${this.multiName}/`;

    this.multiService.getMultiData(this.multiName, this.userName)
      .subscribe((data) => {
        this.multiInfo = data;
        this.title.setTitle( `${data.display_name} subreddits list curated by /u/${this.userName}`);
      });
  }

  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }

  get sortMode() {
    return this.feedService.sortMode;
  }

  set sortMode(sort: string) {
    this.feedService.sortMode = sort;
  }

  get feedSource() {
    return this.feedService.feedSource;
  }

  set feedSource(source: string) {
    this.feedService.feedSource = source;
  }

  get searchQuery() {
    return this.feedService.searchQuery;
  }

  set searchQuery(query: string) {
    this.feedService.searchQuery = query;
  }
}
