import { Component, Input } from '@angular/core';

import { MultiredditService } from 'services/multireddit-service/multireddit.service';

@Component({
  selector: 'sub-list-panel',
  templateUrl: './sub-list-panel.component.html',
  styleUrls: [ './sub-list-panel.component.scss' ]
})

export default class SubListPanelComponent {
  @Input() multiInfo: any;

  private displayLimit: number = 20;
  private limitItems: boolean = true;

  constructor() { }

  toggleLimit() {
    this.limitItems = !this.limitItems;
  }
}
