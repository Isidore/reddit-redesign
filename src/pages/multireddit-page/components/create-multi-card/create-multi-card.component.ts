import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { SubredditService } from 'services/subreddit-service/subreddit.service';
import { MultiredditService } from 'services/multireddit-service/multireddit.service';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'create-multi-card',
  templateUrl: './create-multi-card.component.html',
  styleUrls: [ './create-multi-card.component.scss' ]
})

export default class CreateMultiCardComponent {
  private multiForm: FormGroup;
  private loadingRequest: boolean;
  private responseErrors: any[] = [];
  private submissionSuccess: boolean;

  @Output() onCancel = new EventEmitter<void>();

  constructor(
    private multiService: MultiredditService,
    private subService: SubredditService,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  private subAutoComplete = (input: string): Observable<any> => {
    return this.subService.searchSubNames(input);
  }

  get name() {
    return this.multiForm.get('display_name');
  }

  createForm() {
    this.multiForm = this.fb.group({
      display_name: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern(/^\w+$/)
      ]],

      subreddits: '',
      description_md: '',
      visibility: ['public', Validators.required],
    });
  }

  resetForm() {
    this.multiForm.reset({
      display_name: '',
      subreddits: '',
      description_md: '',
      visibility: 'public'
    });
  }

  submitData() {
    this.loadingRequest = true;
    this.responseErrors = [];
    this.multiForm.disable();

    let multiModel = this.multiForm.value;

    multiModel.subreddits = multiModel.subreddits.length > 0
      ? multiModel.subreddits.map((sub) => ({ name: sub }))
      : [];

    console.log("multiModel: ", multiModel);

    this.multiService.createMulti(multiModel)
      .subscribe(this.handleReponse.bind(this), this.handleError.bind(this));
  }

  handleReponse({ data }) {
    console.log("Success respose: ", data);
    this.multiService.emitAddedMulti(data);

    this.resetForm();
    this.multiForm.enable();
    this.loadingRequest = false;
    this.submissionSuccess = true
  }

  handleError(err) {
    err = err.json();
    this.responseErrors.push(err);

    this.multiForm.enable();
    this.loadingRequest = false;

    console.log("error: ", err);
    console.log("errors: ", this.responseErrors);
  }

  emitCancel() {
    this.onCancel.emit();
  }
}
