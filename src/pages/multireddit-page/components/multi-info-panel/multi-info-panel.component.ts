import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { MultiredditService } from 'services/multireddit-service/multireddit.service';

@Component({
  selector: 'multi-info-panel',
  templateUrl: './multi-info-panel.component.html',
  styleUrls: [ './multi-info-panel.component.scss' ]
})

export default class MultiInfoPanelComponent {
  @Input() multiInfo: any;
  @Input() userName: string;

  // TODO: In our template, we may have to add three element
  // references in order to get rid of hardcored strings
  private activeControl
    : { name: string, error: string }
    = { name: '', error: '' }

  constructor(
    private multiAPI: MultiredditService,
    private sanitizer: DomSanitizer,
    private router: Router
  ) { }

  get multiDescription() {
    var txt = document.createElement('textarea');
    txt.innerHTML = this.multiInfo.description_html;

    return this.sanitizer.bypassSecurityTrustHtml(txt.value);
  }

  toggleActiveControl(controlName: string): void {
    this.activeControl = this.activeControl.name === controlName
      ? { name: '', error: '' }
      : { name: controlName, error: '' };
  }

  changeMultiVisibility(ev) {
    let previousValue = this.multiInfo.visibility;
    this.multiInfo.visibility = ev.target.value;

    this.multiAPI
      .updateMulti(this.multiInfo.path, { visibility: ev.target.value })
      .subscribe(() => {}, () => { this.multiInfo.visibility = previousValue });
  }

  copyMulti(name: string) {
    if (!(/^\w+$/.test(name.trim()))) {
      this.activeControl.error = `Invalid character: '${name.match(/\W/)[0]}'`;
      return
    }

    this.multiAPI.copyMulti(this.multiInfo.path, name)
      .subscribe((data) => {
        this.multiAPI.emitAddedMulti(data);
        this.router.navigate(['user', this.userName, 'm', data.name]);
      });
  }

  // TODO: This one is incomplete
  // TODO: Update URL to the new multi path after submission
  renameMulti(newName: string) {
    if (!(/^\w+$/.test(newName.trim()))) {
      this.activeControl.error = `Invalid character: '${newName.match(/\W/)[0]}'`;
      return
    }

    this.multiAPI.renameMulti(this.multiInfo.path, newName).subscribe(
      (data) => {
        this.multiAPI.emitChangedMulti(this.multiInfo, data);
        this.multiInfo = data; },
      (err) => {
        console.log("renameMulti.err: ", err) });
  }

  deleteMulti() {
    this.multiAPI.deleteMulti(this.multiInfo.path)
      .subscribe((data) => {
        this.multiAPI.emitRemovedMulti(this.multiInfo);
        this.router.navigate(['/'])
      });
  }

  updateDescription(newDescription) {
    let previousValue = this.multiInfo.description_md;
    this.multiInfo.description_md = newDescription;

    this.multiAPI
      .updateDescription(this.multiInfo.path, newDescription)
      .subscribe(
        (data) => {
          this.multiInfo.description_html = data.body_html;
          this.toggleActiveControl(this.activeControl.name);
        },
        (err) => this.multiInfo.description_md = previousValue);
  }

  validateName(name: string): boolean {
    return;
  }
}
