import { Component, Input, Output, EventEmitter } from '@angular/core';

import { togglePageScroll } from 'utils/utils';

var moment = require('moment');

@Component({
  selector: 'multireddit-sidebar',
  templateUrl: './multireddit-sidebar.component.html',
  styleUrls: [ './multireddit-sidebar.component.scss' ]
})

export default class MultiredditSidebarComponent {
  private showOverlay: boolean;
  private activeOverlay: string;

  @Input() multiInfo: any;
  @Input() userName: string;

  private _searchQuery: string;
  @Output() searchQueryChange = new EventEmitter<string>();

  @Input()
  get searchQuery() {
    return this._searchQuery;
  }

  set searchQuery(query: string) {
    this._searchQuery = query;
    this.searchQueryChange.emit(this._searchQuery);
  }

  openOverlay(overlay: string) {
    togglePageScroll(false);
    this.showOverlay = true;
    this.activeOverlay = overlay;
  }

  closeOverlay() {
    togglePageScroll(true);
    this.showOverlay = false;
    this.activeOverlay = '';
  }
}
