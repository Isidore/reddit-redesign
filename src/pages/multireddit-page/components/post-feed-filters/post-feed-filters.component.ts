import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { map } from 'underscore';

@Component({
  selector: 'post-feed-filters',
  templateUrl: './post-feed-filters.component.html',
  styleUrls: [ './post-feed-filters.component.scss' ]
})

export default class PostFeedFiltersComponent {
  @Input() searchMode: boolean;
  @Input() multiInfo: any;

  private subOptions: object[] = [];

  private _sortMode: string;
  private _feedSource: string;

  @Output() sortModeChange = new EventEmitter<string>();
  @Output() feedSourceChange = new EventEmitter<string>();

  ngOnInit() {
    this.subOptions = map(this.multiInfo.subreddits,
      (sub) => ({ label: sub.name, value: `r/${sub.name}` }));
    this.subOptions
      .unshift({ label: 'all', value: this.multiInfo.path, selected: true });
  }

  @Input()
  get sortMode() {
    return this._sortMode;
  }

  set sortMode(sort: string) {
    this._sortMode = sort;
    this.sortModeChange.emit(this._sortMode);
  }

  @Input()
  get feedSource() {
    return this._feedSource;
  }

  set feedSource(source: string) {
    this._feedSource = source;
    this.feedSourceChange.emit(this._feedSource);
  }
}
