import { NgModule }            from '@angular/core';
import { RouterModule }        from '@angular/router';

import { HomePageModule }      from './home-page/home.module';
import { SubredditPageModule } from './subreddit-page/subreddit.module';
import { MultiredditPageModule } from './multireddit-page/multireddit.module';

import { routes }              from './pages.routes';

@NgModule({
  imports: [
    RouterModule.forChild(routes),

    HomePageModule,
    SubredditPageModule,
    MultiredditPageModule
  ],

  exports: [
    HomePageModule,
    SubredditPageModule,
    MultiredditPageModule
  ]
})

export class PagesModule { }
