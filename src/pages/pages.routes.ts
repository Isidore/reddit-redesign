import { Routes } from '@angular/router';

import HomePage from './home-page/home.page';
import SubredditPage from './subreddit-page/subreddit.page';
import MultiredditPage from './multireddit-page/multireddit.page';

export const routes: Routes = [
  { path: ''
  , component: HomePage
  , data: { title: 'reddit: the front page of the internet' }
  }
  ,
  { path: 'login'
  , redirectTo: '/'
  , pathMatch: 'full'
  }
  ,
  { path: 'user/:userName/m/:multiName'
  , component: MultiredditPage
  , data: { title: '' }
  }
];
