import { NgModule }             from '@angular/core';
import { FormsModule }          from '@angular/forms';
import { CommonModule }         from '@angular/common';

import { ComponentsModule }     from 'components/components.module';

import HomePage                 from './home.page';
import UserActionsComponent     from './components/user-actions';
import PostFeedFiltersComponent from './components/post-feed-filters';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    ComponentsModule
  ],

  declarations: [
    HomePage,
    UserActionsComponent,
    PostFeedFiltersComponent
  ],

  exports: [ HomePage ]
})

export class HomePageModule { }
