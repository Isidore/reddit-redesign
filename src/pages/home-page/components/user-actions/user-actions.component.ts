import { Component, Input, Output, EventEmitter } from '@angular/core';

import { togglePageScroll } from 'utils/utils';

var moment = require('moment');

@Component({
  selector: 'user-actions',
  templateUrl: './user-actions.component.html',
  styleUrls: [ './user-actions.component.scss' ]
})

export default class UserActionsComponent {
  private showOverlay: boolean = false;
  private activeOverlay: string;

  private _searchQuery: string;
  @Output() searchQueryChange = new EventEmitter<string>();

  @Input()
  get searchQuery() {
    return this._searchQuery;
  }

  set searchQuery(query: string) {
    this._searchQuery = query;
    this.searchQueryChange.emit(this._searchQuery);
  }

  get redditGold() {
    let percentage = moment.duration(moment().format('h:mm:ss')).asSeconds();

    return (percentage / 46764).toFixed(2);
  }

  animateRedditGold(state, circle) {
    let value = Math.round(circle.value() * 100);
    circle.setText(value + '%');
  }

  openOverlay(overlay: string) {
    togglePageScroll(false)
    this.showOverlay = true;
    this.activeOverlay = overlay;
  }

  closeOverlay() {
    togglePageScroll(true)
    this.showOverlay = false;
    this.activeOverlay = '';
  }
}
