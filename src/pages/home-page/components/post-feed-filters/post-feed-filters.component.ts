import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'post-feed-filters',
  templateUrl: './post-feed-filters.component.html',
  styleUrls: [ './post-feed-filters.component.scss' ]
})

export default class PostFeedFiltersComponent {
  private _sortMode: string;
  private _feedSource: string;

  @Input() searchMode: boolean;
  @Input() sources: string[];

  @Output() sortModeChange = new EventEmitter<string>();
  @Output() feedSourceChange = new EventEmitter<string>();

  mapSources() {
    return this.sources
      .map((src, i) => ({ label: src[0], value: src[1], selected: i === 0 }));
  }

  @Input()
  get sortMode() {
    return this._sortMode;
  }

  @Input()
  get feedSource() {
    return this._feedSource;
  }

  set sortMode(sort: string) {
    this._sortMode = sort;
    this.sortModeChange.emit(this._sortMode);
  }

  set feedSource(source: string) {
    this._feedSource = source;
    this.feedSourceChange.emit(this._feedSource);
  }
}
