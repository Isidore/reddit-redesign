import { Component, OnInit } from '@angular/core';

import { FeedService } from 'services/feed-service/feed.service';
import { UserService } from 'services/user-service/user.service';

@Component({
  templateUrl: './home.page.html',
  providers: [ FeedService ]
})

export default class HomePage implements OnInit {
  private availableSources: Array<string>[] = [];

  constructor(
    private feedService: FeedService,
    private user: UserService
  ) { }

  ngOnInit() {
    this.availableSources = this.user.isAuthenticated()
      ? [['Home', 'home'], ['Saved', 'saved'], ['all', 'r/all'], ['popular', 'r/popular']]
      : [['all', 'r/all'], ['popular', 'r/popular']];

    this.sortMode = 'hot';
    this.feedSource = this.availableSources[0][1];
    this.searchQuery = '';

    this.feedService.displaySub = true;
  }

  get sortMode() {
    return this.feedService.sortMode;
  }

  set sortMode(sort: string) {
    this.feedService.sortMode = sort;
  }

  get feedSource() {
    return this.feedService.feedSource;
  }

  set feedSource(source: string) {
    this.feedService.feedSource = source;
  }

  get searchQuery() {
    return this.feedService.searchQuery;
  }

  set searchQuery(query: string) {
    this.feedService.searchQuery = query;
  }
}
