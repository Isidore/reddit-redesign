import { NgModule }             from '@angular/core';
import { RouterModule }         from '@angular/router';
import { CommonModule }         from '@angular/common';
import { FormsModule }          from '@angular/forms';

import { ComponentsModule }     from 'components/components.module';

import SubredditPage            from './subreddit.page';
import { routes }               from './subreddit.routes';

import SubredditHeaderComponent from './components/subreddit-header';
import CommentsFeedComponent    from './components/comments-feed';
import PostCommentsComponent    from './components/post-comments';

import SubredditPanels          from './components/subreddit-panels';
import SubRulesPanelComponent   from './components/sub-rules-panel';
import SubInfoPanelComponent    from './components/sub-info-panel';
import SubModsPanelComponent    from './components/sub-mods-panel';

@NgModule({
  imports: [
    RouterModule.forChild(routes),

    CommonModule,
    FormsModule,

    ComponentsModule
  ],

  declarations: [
    SubredditPage,
    SubredditHeaderComponent,
    CommentsFeedComponent,
    PostCommentsComponent,

    SubredditPanels,
    SubRulesPanelComponent,
    SubInfoPanelComponent,
    SubModsPanelComponent
  ],

  exports: [ SubredditPage ]
})

export class SubredditPageModule { }
