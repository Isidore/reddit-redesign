import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

import { SubredditService } from 'services/subreddit-service/subreddit.service';

@Component({
  selector: 'sub-rules-panel',
  templateUrl: './sub-rules-panel.component.html',
  styleUrls: [ './sub-rules-panel.component.scss' ]
})

export default class SubRulesPanelComponent implements OnChanges, OnInit {
  @Input() subName: string;

  private subRules: object[] = [];
  private displayLimit: number = 20;
  private limitItems: boolean = true;

  constructor( private subService: SubredditService ) { }

  ngOnInit() {
    this.limitItems = true;
    this.fetchSubRules();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.subName && !changes.subName.firstChange)
      this.ngOnInit();
  }

  fetchSubRules(): void {
    this.subService.getSubRules(this.subName).subscribe((data) => {
      this.subRules = data.rules;
    });
  }

  toggleLimit() {
    this.limitItems = !this.limitItems;
  }
}
