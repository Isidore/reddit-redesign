import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'comments-feed',
  templateUrl: './comments-feed.component.html',
  styleUrls: [ './comments-feed.component.scss' ]
})

export default class CommentsFeedComponent {
  @Input() comments: object[];
  @Input() sortMode: string;
  @Input() numComments: number;
  @Input() nextComments: object;

  @Output() onChangeSortMode = new EventEmitter<string>();
  @Output() onCommentAction = new EventEmitter<object>();
  @Output() onLoadMore = new EventEmitter<void>();

  changeSortMode(mode: string): void {
    this.onChangeSortMode.emit(mode);
  }

  loadMore(): void {
    this.onLoadMore.emit();
  }

  emitCommentAction(data): void {
    this.onCommentAction.emit(data);
  }
}
