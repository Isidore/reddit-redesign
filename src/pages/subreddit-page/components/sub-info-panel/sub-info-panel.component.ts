import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { UserService } from 'services/user-service/user.service';
import { SubredditService } from 'services/subreddit-service/subreddit.service';

import { abbreviate } from 'utils/utils';

@Component({
  selector: 'sub-info-panel',
  templateUrl: './sub-info-panel.component.html',
  styleUrls: [ './sub-info-panel.component.scss' ]
})

export default class SubInfoPanelComponent {
  @Input() subInfo: any;

  constructor(
    private subService: SubredditService,
    private userService: UserService,
    private sanitizer: DomSanitizer,
   ) { }

  get activeUsers() {
    return this.subInfo.accounts_active.toLocaleString();
  }

  get subDescription() {
    var txt = document.createElement('textarea');
    txt.innerHTML = this.subInfo.public_description_html;

    return this.sanitizer.bypassSecurityTrustHtml(txt.value);
  }

  get userIsAuthenticated() {
    return this.userService.isAuthenticated();
  }

  toggleUserFlair(ev): void {
    let lastFlairValue = this.subInfo.user_sr_flair_enabled;
    this.subInfo.user_sr_flair_enabled = ev.target.checked;

    this.subService.setUserFlair(this.subInfo.display_name, ev.target.checked)
      .subscribe(() => {}, (err) => this.subInfo.user_sr_flair_enabled = lastFlairValue);
  }
}
