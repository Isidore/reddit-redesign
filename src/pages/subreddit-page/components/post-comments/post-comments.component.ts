import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { PostsService } from 'services/posts-service/posts.service';

import { findWhere, where } from 'underscore';
import { togglePageScroll } from 'utils/utils'

@Component({
  selector: 'post-comments',
  templateUrl: './post-comments.component.html',
  styleUrls: [ './post-comments.component.scss' ]
})

export default class PostCommentsComponent implements OnInit {
  private isLoading: boolean = true;

  private post: any;
  private postComments: Array<object>;
  private commentsSortMode: string = 'confidence';
  private nextComments: any;

  private showOverlay: boolean;
  private overlayData
    : { action: string, thing: object }
    = { action: '', thing: { } };

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title
  ) { }

  ngOnInit() {
    this.fetchComments();
  }

  fetchComments(): void {
    let subName = this.route.parent.snapshot.params.subredditName;
    let postId = this.route.snapshot.params.postId;
    let options =
      { sr_detail: false
      , showmore: true
      , depth: 20
      , context: 8
      , sort: this.commentsSortMode
      };

    this.postsService.getPostComments(subName, postId, options)
      .subscribe(({ post, comments }) => {
        this.post = post;
        this.postComments = where(comments, { kind: 't1' });
        this.nextComments = findWhere(comments, { kind: 'more' }) || { };
        this.isLoading = false;

        console.log("MoreChildren: ", this.nextComments);
      });
  }

  loadMoreComments(): void {
    this.postsService
      .getMoreComments(this.post.name, this.nextComments.data.children, this.commentsSortMode)
      .subscribe(({ comments, moreChildren }) => {
        console.log("loadMore.comments: ", comments);
        console.log("loadMore.moreChildren: ", moreChildren);

        this.postComments.push(...comments);
        this.nextComments = moreChildren;

        console.log("this.postComments: ", this.postComments);
      });
  }

  openOverlay(overlayData): void {
    togglePageScroll(false);
    this.showOverlay = true;
    this.overlayData = overlayData;
  }

  closeOverlay(): void {
    togglePageScroll(true);
    this.showOverlay = false;
    this.overlayData = { action: '', thing: {} };
  }

  addComment(comment): void {
    this.post.num_comments = this.post.num_comments++;
    this.postComments.unshift(comment);
  }

  changeSortMode(sortMode: string): void {
    this.commentsSortMode = sortMode;
    this.fetchComments();
  }
}
