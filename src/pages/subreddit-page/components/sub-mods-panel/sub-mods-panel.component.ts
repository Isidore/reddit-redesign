import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

import { SubredditService } from 'services/subreddit-service/subreddit.service';

@Component({
  selector: 'sub-mods-panel',
  templateUrl: './sub-mods-panel.component.html',
  styleUrls: [ './sub-mods-panel.component.scss' ]
})

export default class SubModsPanelComponent implements OnChanges, OnInit {
  @Input() subName: string;

  private subMods: object[] = [];
  private displayLimit: number = 20;
  private limitItems: boolean = true;

  constructor(private subService: SubredditService) { }

  ngOnInit() {
    this.limitItems = true;
    this.fetchSubMods();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.subName && !changes.subName.firstChange)
      this.ngOnInit();
  }

  fetchSubMods(): void {
    this.subService.getSubMods(this.subName).subscribe((data) => {
      this.subMods = data.children;
    })
  }

  toggleLimit() {
    this.limitItems = !this.limitItems;
  }
}
