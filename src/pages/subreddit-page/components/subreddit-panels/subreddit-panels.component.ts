import { Component, Input } from '@angular/core';

@Component({
  selector: 'subreddit-panels',
  templateUrl: './subreddit-panels.component.html',
  styleUrls: [ './subreddit-panels.component.scss' ]
})

export default class SubredditPanelsComponent {
  @Input() subName: string;
  @Input() subInfo: any;
}
