import {
  Component,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';

import { scoper } from 'utils/utils';

import { SubredditService } from 'services/subreddit-service/subreddit.service';

import { abbreviate, togglePageScroll } from 'utils/utils';

@Component({
  selector: 'subreddit-header',
  templateUrl: './subreddit-header.component.html',
  styleUrls: [ './subreddit-header.component.scss' ]
})

export default class SubredditHeaderComponent implements OnChanges, AfterViewInit, OnDestroy {
  @ViewChild('srHeader') srHeader: ElementRef;

  private subStyleTag: any;
  private activeSubmitForm: boolean = false;

  @Input() subName: string;;
  @Input() subInfo: any;

  private _sortMode: string;
  private _searchQuery: string;

  @Output() sortModeChange = new EventEmitter<string>();
  @Output() searchQueryChange = new EventEmitter<string>();

  constructor(private subService: SubredditService) { }

  ngOnChanges(changes: SimpleChanges) {
    console.log("SubredditHeader.onChanges: ", changes);

    if (changes.subName && !changes.subName.firstChange) {
      this.ngOnDestroy();
      this.ngAfterViewInit();
    }
  }

  ngAfterViewInit() {
    console.log("running AfterViewInit...");

    this.subService.getSubStyle(this.subName)
      .subscribe((stylesheet) => {
        this.subStyleTag = document.createElement('style');
        let docHead = document.head || document.getElementsByTagName('head')[0];
        let srHeaderId = '#' + this.srHeader.nativeElement.id;

        let scopedStyle = document.createTextNode(scoper(stylesheet, srHeaderId));

        this.subStyleTag.appendChild(scopedStyle);
        docHead.appendChild(this.subStyleTag);
      });
  }

  ngOnDestroy() {
    console.log("running OnDestroy....")

    let docHead = document.head || document.getElementsByTagName('head')[0];
    docHead.removeChild(this.subStyleTag);
    this.subStyleTag = undefined;
  }

  @Input()
  get sortMode() {
    return this._sortMode;
  }

  set sortMode(sort: string) {
    this._sortMode = sort;
    this.sortModeChange.emit(this._sortMode);
  }

  @Input()
  get searchQuery() {
    return this._searchQuery;
  }

  set searchQuery(query: string) {
    this._searchQuery = query;
    this.searchQueryChange.emit(this._searchQuery);
  }

  get subscriberCount() {
    return abbreviate(this.subInfo.subscribers, 0);
  }

  get headerCaption() {
    return this.subInfo.header_img ? '' : 'reddit.com';
  }

  subscribeUser(): void {
    let initialState = this.subInfo.user_is_subscriber;
    let action = this.subInfo.user_is_subscriber ? 'unsub' : 'sub';
    let sr = this.subInfo.name;

    this.subInfo.user_is_subscriber = action === 'sub' ? true : false;

    this.subService.subscribeUser(sr, action)
      .subscribe(
        () => { },
        (err) => this.subInfo.user_is_subscriber = initialState)
  }

  toggleSubmitForm(formState: boolean) {
    togglePageScroll(!formState);
    this.activeSubmitForm = formState;
  }
}
