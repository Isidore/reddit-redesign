import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { SubredditService } from 'services/subreddit-service/subreddit.service';
import { FeedService } from 'services/feed-service/feed.service';

import { ISubscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/skip';

@Component({
  templateUrl: './subreddit.page.html',
  providers: [ FeedService ]
})

export default class SubredditPage implements OnInit, OnDestroy {
  private subName: string = '';
  private subInfo: any;
  private routerSubscription: ISubscription;

  constructor(
    private subService: SubredditService,
    private feedService: FeedService,
    private route: ActivatedRoute,
    private title: Title,
    private router: Router
  ) {
    this.routerSubscription = router.events
      .skip(1)
      .filter(ev => ev instanceof NavigationEnd)
      .subscribe((ev: NavigationEnd) => {
        if (/^\/r\/\w+$/.test(ev.url))
          this.ngOnInit();
      })
  }

  ngOnInit() {
    this.subName = this.route.snapshot.params.subredditName;
    this.feedService.displaySub = false;

    this.feedSource = `r/${this.subName}`;
    this.sortMode = 'hot';
    this.searchQuery = '';

    this.subService.getSubInfo(this.subName).subscribe((res) => {
      this.subInfo = res;
      this.title.setTitle(res.title || res.display_name);
    });
  }

  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }

  get sortMode() {
    return this.feedService.sortMode;
  }

  set sortMode(sort: string) {
    this.feedService.sortMode = sort;
  }

  get feedSource() {
    return this.feedService.feedSource;
  }

  set feedSource(source: string) {
    this.feedService.feedSource = source;
  }

  get searchQuery() {
    return this.feedService.searchQuery;
  }

  set searchQuery(query: string) {
    this.feedService.searchQuery = query;
  }
}
