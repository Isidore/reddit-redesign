import { Routes } from '@angular/router';

import SubredditPage from './subreddit.page';

import PostCommentsComponent from './components/post-comments';
import PostFeedComponent from 'components/post-feed';

export const routes: Routes = [
  { path: 'r/:subredditName'
  , component: SubredditPage
  , children:
    [
    { path: ''
    , component: PostFeedComponent
    , data: { title: ' ' }
    }
    ,
    { path: 'comments/:postId/:postSlug'
    , component: PostCommentsComponent
    , data: { title: ' ' }
    }
    ]
  }
];
